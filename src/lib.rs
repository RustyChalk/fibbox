
use std::fmt;
use rug::Integer;
use rug::ops::Pow;

/// An alias for rug::Integer
/// Descends from crate `gmp-mpfr-sys` implementing GMP and MPFR.
type Mpz = Integer;

/// The Fibonacci Box, or FibBox struct contains 4 members: f1, dif, ctr, f2
///
/// <table>
/// <tr><td>f1</td><td>Mpz, Integer</td><td>Lesser Factor</td>
/// <td>f2-2*dif</td>
/// <td>ctr-dif</td>
/// </tr>
/// <tr><td>dif</td><td>Mpz, Integer</td><td>Difference of Squares</td>
/// <td>|(f2-f1)/2|</td>
/// <td>f2-ctr</td>
/// </tr>
/// <tr><td>ctr</td><td>Mpz, Integer</td><td>Centered value</td>
/// <td>|(f2+f1)/2|</td>
/// <td>f1+dif</td>
/// </tr>
/// <tr><td>f2</td><td>Mpz, Integer</td><td>Greater Factor</td>
/// <td>f1+2*dif</td>
/// <td>dif+ctr</td>
/// </tr>
/// </table>
///
/// The individual members are prviate. To access the individual members, use similarly named
/// methods..
///
/// Usage:
///
/// ```rust
/// use fibbox::FibBox;
/// let fb = FibBox::new(1, 1, 2, 3);
/// let f1 = fb.f1();
/// let dif = fb.dif();
/// let ctr = fb.ctr();
/// let f2 = fb.f2();
/// ```
///
/// Each of the access methods is a borrow from the struct's private members.
///
/// In addition, the core usage behind a Fibonacci Box is its relationship to Pythagorean Triples
///
/// ```rust
/// use rug::Integer;
/// use rug::ops::Pow;
/// use fibbox::FibBox;
/// let fb = FibBox::new(1, 1, 2, 3);
/// let pt = fb.as_pytup(); // This is a tuple, 3 members, a pythagorean triple.
/// assert_eq!(Integer::from(fb.f1()*fb.f2()), pt.0);
/// let dos = Integer::from(2);
/// let csq = Integer::pow(Integer::from(fb.ctr()), 2);
/// let dsq = Integer::pow(Integer::from(fb.dif()), 2);
/// let cd = Integer::from(fb.ctr()*fb.dif());
/// assert_eq!(Integer::from(&csq-&dsq), pt.0);
/// assert_eq!(Integer::from(&dos*&cd), pt.1);
/// assert_eq!(Integer::from(&csq+&dsq), pt.2);
/// ```
///
#[derive(Clone, PartialEq)]
pub struct FibBox {
    f1: Mpz,
    dif: Mpz,
    ctr: Mpz,
    f2: Mpz,
}

impl fmt::Display for FibBox {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {} {}", self.f1, self.dif, self.ctr, self.f2)
    }
}

impl fmt::Debug for FibBox {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {} {}", self.f1, self.dif, self.ctr, self.f2)
    }
}

impl FibBox {
    /// If the `gmp-mpfr-sys` library supports conversion, 
    /// this new function will support conversion from whichever types are input.
    ///
    /// ```rust
    /// use rug::Integer;
    /// use fibbox::FibBox;
    /// let fb = FibBox::new(1i8, 1u16, 2i32, 3u64);   // Works
    /// let fb = FibBox::new(1i16, 1u32, 2u64, 3i128); // Works
    /// let uno = Integer::from(1u8);
    /// let dos = Integer::from(2i32);
    /// let tres = Integer::from(3u128);
    /// let fb = FibBox::new(&uno, &uno, dos, tres);   // Works
    /// ```
    pub fn new<T: Into<Mpz>, U: Into<Mpz>, V: Into<Mpz>, W: Into<Mpz>>(f1: T, dif: U, ctr: V, f2: W) -> FibBox {
        let tup: (Mpz, Mpz, Mpz, Mpz) = (f1.into(), dif.into(), ctr.into(), f2.into());
        let two = Mpz::from(2);
        assert!(&tup.3 > &tup.0);
        assert!(&tup.2 > &tup.1);
        assert!(Mpz::from(&tup.0+&tup.1) == tup.2, "{:?}", tup);
        assert_eq!(Mpz::from(&tup.1+&tup.2), tup.3, "{:?}", tup);
        assert!((Mpz::from(&tup.3+&tup.0)).div_exact(&two) == tup.2);
        assert!((Mpz::from(&tup.3-&tup.0)).div_exact(&two) == tup.1);
        FibBox {
            f1: tup.0,
            dif: tup.1,
            ctr: tup.2,
            f2: tup.3,
        }
    }

    /// Accepts a tuple of 4 integers and returns a new instance.
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let tup = (1, 2, 3, 5);
    /// let fb = FibBox::new_from_tuple(tup);
    /// println!("{:?}", fb.as_pytup());
    /// println!("{:?}", fb.as_tuple());
    /// println!("{:?}", fb);
    /// // Output:
    /// // (5, 12, 13)
    /// // (1, 2, 3, 5)
    /// // 1 1 2 3
    pub fn new_from_tuple<T: Into<Mpz>, U: Into<Mpz>, V: Into<Mpz>, W: Into<Mpz>>(tuvw: (T, U, V, W)) -> FibBox {
        FibBox::new(tuvw.0, tuvw.1, tuvw.2, tuvw.3)
    }

    /// Returns a new instance from 3 integer inputs, representing a Pythagorean Triple.
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let fb = FibBox::new_from_pytrip(3, 4, 5);
    /// println!("{:?}", fb);
    /// println!("{:?}", fb.as_pytup());
    /// // Output:
    /// // 1 1 2 3
    /// // (3, 4, 5)
    /// ```
    pub fn new_from_pytrip<A: Into<Mpz>, B: Into<Mpz>, C: Into<Mpz>>(a: A, b: B, c: C) -> FibBox {
        let two = Mpz::from(2);
        let mut _tup: (Mpz, Mpz, Mpz) = (a.into(), b.into(), c.into());
        assert!(&_tup.0 < &_tup.2, "The first two members of the tuple must be less than the third.");
        assert!(&_tup.1 < &_tup.2, "The first two members of the tuple must be less than the third.");
        let _asq = Integer::pow(Integer::from(&_tup.0), 2);
        let _bsq = Integer::pow(Integer::from(&_tup.1), 2);
        let _csq = Integer::pow(Integer::from(&_tup.2), 2);
        assert!(Integer::from(&_asq+&_bsq) == _csq, "a^2+b^2!=c^2");
        let mut _f1 = Mpz::from(0);
        let mut _dif = Mpz::from(0);
        let mut _ctr = Mpz::from(0);
        let mut _f2 = Mpz::from(0);
        let _gcd = Mpz::from(&_tup.0).gcd(&_tup.1);
        if _gcd > 1 {
            let _tmp0 = Mpz::from(&_tup.0).div_exact(&_gcd);
            let _tmp1 = Mpz::from(&_tup.1).div_exact(&_gcd);
            if _tmp0.mod_u(2) == _tmp1.mod_u(2) {
                panic!("After removing GCD, a and b cannot be the same (mod 2).");
            } else {
                if _tmp1.mod_u(2) == 1 {
                    _tup = (_tup.1, _tup.0, _tup.2);
                }
            }
        } else {
            if (&_tup.1).mod_u(2) == 1 {
                _tup = (_tup.1, _tup.0, _tup.2);
            }
        }
        assert!(Mpz::from(&_tup.2-&_tup.0).div_exact(&two).is_perfect_square(), "(c-a)/2 or (c-b)/2 must be square!");
        _dif = ((Mpz::from(&_tup.2-&_tup.0)).div_exact(&two)).sqrt();
        assert!(Mpz::from(&_tup.1).is_divisible(&Mpz::from(&two*&_dif)), "b/(2d) must be divisible by difference of squares! {:?}", _tup);
        _ctr = (Mpz::from(&_tup.1)).div_exact(&Mpz::from(&two*&_dif));
        _f1 = Mpz::from(&_ctr-&_dif);
        _f2 = Mpz::from(&_ctr+&_dif);
        FibBox::new(_f1, _dif, _ctr, _f2)
    }

    /// Accepts a tuple of 3 integers, representing a Pythagorean Triple, 
    /// and returns a new instance.
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let tup = (3, 4, 5);
    /// let fb = FibBox::new_from_pytrip_tuple(tup);
    /// println!("{:?}", fb.as_pytup());
    /// println!("{:?}", fb);
    /// // Output:
    /// // (3, 4, 5)
    /// // 1 1 2 3
    /// ```
    pub fn new_from_pytrip_tuple<A: Into<Mpz>, B: Into<Mpz>, C: Into<Mpz>>(abc: (A, B, C)) -> FibBox {
        FibBox::new_from_pytrip(abc.0, abc.1, abc.2)
    }

    /// Returns the struct's members as a tuple (of 4 integers)
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let fb = FibBox::new(1, 1, 2, 3);
    /// let tup = fb.as_tuple();
    /// println!("{:?}", tup);
    /// // Output:
    /// // (1, 1, 2, 3)
    /// ```
    pub fn as_tuple(&self) -> (Mpz, Mpz, Mpz, Mpz) {
        (Mpz::from(&self.f1), Mpz::from(&self.dif), Mpz::from(&self.ctr), Mpz::from(&self.f2))
    }

    /// Returns a 3 member tuple, (A, B, C), such that A^2+B^2==C^2
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let fb = FibBox::new(1, 1, 2, 3);
    /// let pt = fb.as_pytup();
    /// println!("{:?}", pt);
    /// // Output:
    /// // (3, 4, 5)
    /// ```
    pub fn as_pytup(&self) -> (Mpz, Mpz, Mpz) {
        let ctr = Mpz::from(&self.ctr);
        let dif = Mpz::from(&self.dif);
        let b = Mpz::from(Mpz::from(2)*(&ctr)*(&dif));
        let ctr2 = Mpz::pow(ctr, 2);
        let dif2 = Mpz::pow(dif, 2);
        let a = Mpz::from(&ctr2-&dif2);
        let c = Mpz::from(&ctr2+&dif2);
        (a, b, c)
    }

    /// Returns true or false, whether or not the Fibonacci Box 
    /// represents a primitive Pythagorean Triple or not.
    ///
    /// Usage:
    /// ```rust
    /// use fibbox::FibBox;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// println!("{:?}", fb.is_primitive()); // true
    /// assert!(fb.gcd() == 1);
    /// let np = FibBox::new(9, 3, 12, 15); // (135, 72, 153)
    /// println!("{:?}", np.is_primitive()); // true
    /// assert!(np.gcd() == 3);
    /// ```
    pub fn is_primitive(&self) -> bool {
        (Mpz::from(&self.f1)).gcd(&self.f2) == 1
    }

    /// Returns the GCD; 1 if primitive, else a positive integer.
    ///
    /// Usage:
    /// ```rust
    /// // Same example as `is_primitive(&self)`
    /// use fibbox::FibBox;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// println!("{:?}", fb.is_primitive()); // true
    /// assert!(fb.gcd() == 1);
    /// let np = FibBox::new(9, 3, 12, 15); // (135, 72, 153)
    /// println!("{:?}", np.is_primitive()); // false
    /// assert!(np.gcd() == 3);
    /// ```
    pub fn gcd(&self) -> Mpz {
        (Mpz::from(&self.f1)).gcd(&self.f2)
    }

    /// Returns the default base or root
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let a = FibBox::new(1, 1, 2, 3);
    /// let b = FibBox::Root();
    /// assert_eq!(a, b);
    /// ```
    #[allow(non_snake_case)]
    pub fn Root() -> FibBox {
        FibBox::new(1, 1, 2, 3)
    }

    /// Returns the value of f1 (borrowed).
    ///
    /// Usage:
    /// ```rust
    /// use rug::Integer;
    /// use fibbox::FibBox;
    /// type Mpz = Integer;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// assert_eq!(Mpz::from(fb.f1()), 1);
    /// ```
    pub fn f1(&self) -> &Mpz {
        &self.f1
    }
    
    /// Returns the value of dif (borrowed).
    ///
    /// Usage:
    /// ```rust
    /// use rug::Integer;
    /// use fibbox::FibBox;
    /// type Mpz = Integer;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// assert_eq!(Mpz::from(fb.dif()), 1);
    /// ```
    pub fn dif(&self) -> &Mpz {
        &self.dif
    }
    
    /// Returns the value of ctr (borrowed).
    ///
    /// Usage:
    /// ```rust
    /// use rug::Integer;
    /// use fibbox::FibBox;
    /// type Mpz = Integer;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// assert_eq!(Mpz::from(fb.ctr()), 2);
    /// ```
    pub fn ctr(&self) -> &Mpz {
        &self.ctr
    }
    
    /// Returns the value of f2 (borrowed).
    ///
    /// Usage:
    /// ```rust
    /// use rug::Integer;
    /// use fibbox::FibBox;
    /// type Mpz = Integer;
    /// let fb = FibBox::new(1, 1, 2, 3); // (3, 4, 5)
    /// assert_eq!(Mpz::from(fb.f2()), 3);
    /// ```
    pub fn f2(&self) -> &Mpz {
        &self.f2
    }

    /// Mutates the struct following the [Price _(2008)_](https://arxiv.org/pdf/0809.4324.pdf) A branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::new(1, 1, 2, 3);
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.price_a();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.price_a();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.price_a();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.price_a();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// // Output:
    /// // 1 1 2 3 (3, 4, 5)
    /// // 1 2 3 5 (5, 12, 13)
    /// // 1 4 5 9 (9, 40, 41)
    /// // 1 8 9 17 (17, 144, 145)
    /// // 1 16 17 33 (33, 544, 545)
    /// ```
    ///
    pub fn price_a(&mut self) {
        self.ctr = Mpz::from(&self.f2);
        self.dif = Mpz::from(&self.ctr-&self.f1);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [Price _(2008)_](https://arxiv.org/pdf/0809.4324.pdf) B branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::new(1, 1, 2, 3);
    /// let pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_b();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_b();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_b();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_b();
    /// println!("{} {:?}", fb, pt);
    /// // Output: 
    /// // 1 1 2 3 (3, 4, 5)
    /// // 3 1 4 5 (15, 8, 17)
    /// // 5 3 8 11 (55, 48, 73)
    /// // 11 5 16 21 (231, 160, 281)
    /// // 21 11 32 43 (903, 704, 1145)
    /// ```
    ///
    pub fn price_b(&mut self) {
        self.dif = Mpz::from(&self.f1);
        self.f1 = Mpz::from(&self.f2);
        self.ctr = Mpz::from(&self.dif+&self.f1);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [Price _(2008)_](https://arxiv.org/pdf/0809.4324.pdf) C branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::Root();
    /// let mut pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// // Output:
    /// // 1 1 2 3 (3, 4, 5)
    /// // 1 3 4 7 (7, 24, 25)
    /// // 1 7 8 15 (15, 112, 113)
    /// // 1 15 16 31 (31, 480, 481)
    /// // 1 31 32 63 (63, 1984, 1985)
    /// ```
    ///
    pub fn price_c(&mut self) {
        self.dif = Mpz::from(&self.f2);
        self.ctr = Mpz::from(&self.dif+&self.f1);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [Price _(2008)_](https://arxiv.org/pdf/0809.4324.pdf) tree,
    /// becoming the parent (tree) value.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::Root();
    /// let mut pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_b(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.price_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// assert!(fb == FibBox::Root());
    /// // Output:
    /// // 1 1 2 3 (3, 4, 5)
    /// // 1 3 4 7 (5, 12, 13)
    /// // 1 7 8 15 (35, 12, 37)
    /// // 5 7 12 19 (95, 168, 193)
    /// // 5 1 6 7 (35, 12, 37)
    /// // 1 2 3 5 (5, 12, 13)
    /// // 1 1 2 3 (3, 4, 5)
    /// ```
    pub fn price_p(&mut self) {
        if self != &FibBox::Root() {
            let two = Mpz::from(2);
            let _gcd = (Mpz::from(&self.f1)).gcd(&self.f2);
            let mut _branch = "";
            if _gcd > 1 {
                let _f1 = (Mpz::from(&self.f1)).div_exact(&_gcd);
                let _f2 = (Mpz::from(&self.f2)).div_exact(&_gcd);
                let _dif = (Mpz::from(&_f2-&_f1)).div_exact(&two);
                //let _ctr = Mpz::from(&_f1+&_dif); // Not needed.
                if _dif.mod_u(2) == 1 {
                    // b or c branch
                    if &_dif < &_f1 {
                        // b branch
                        _branch = "b";
                    } else {
                        // c branch
                        _branch = "c";
                    }
                } else {
                    // a branch
                    _branch = "a";
                }
            } else {
                if self.dif.mod_u(2) == 1 {
                    // b or c branch
                    if &self.dif < &self.f1 {
                        // b branch
                        _branch = "b";
                    } else {
                        // c branch
                        _branch = "c";
                    } 
                } else {
                    _branch = "a"
                }
            }
            match _branch {
                "a" => {
                    self.f2 = Mpz::from(&self.ctr);
                    self.ctr = (Mpz::from(&self.f2+&self.f1)).div_exact(&two);
                    assert!(&self.f2 > &self.ctr);
                    self.dif = Mpz::from(&self.f2-&self.ctr);
                    assert!(&self.ctr > &self.dif);
                }, 
                "b" => {
                    self.f2 = Mpz::from(&self.f1);
                    self.f1 = Mpz::from(&self.dif);
                    self.dif = (Mpz::from(&self.f2-&self.f1)).div_exact(&two);
                    self.ctr = Mpz::from(&self.f1+&self.dif);
                },
                "c" => {
                    self.f2 = Mpz::from(&self.dif);
                    self.dif = (Mpz::from(&self.f2-&self.f1)).div_exact(&two);
                    self.ctr = Mpz::from(&self.f1+&self.dif);
                },
                _ => {
                    panic!("No branch designated!");
                },
            }
        }
    }

    /// Mutates the struct following the [B. Berggren _(1934)_](https://arxiv.org/pdf/0809.4324.pdf) A branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::new(1, 1, 2, 3);
    /// let mut pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// // Output: 
    /// // 1 1 2 3 (3, 4, 5)
    /// // 3 1 4 5 (15, 8, 17)
    /// // 5 1 6 7 (35, 12, 37)
    /// // 7 1 8 9 (63, 16, 65)
    /// // 9 1 10 11 (99, 20, 101)
    /// ```
    ///
    pub fn berggren_a(&mut self) {
        self.f1 = Mpz::from(&self.f2);
        self.ctr = Mpz::from(&self.f1+&self.dif);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [B. Berggren _(1934)_](https://arxiv.org/pdf/0809.4324.pdf) B branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::Root();
    /// let pt = fb.as_pytup();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.berggren_b();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.berggren_b();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.berggren_b();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// fb.berggren_b();
    /// println!("{} {:?}", fb, fb.as_pytup());
    /// // Output:
    /// // 1 1 2 3 (3, 4, 5)
    /// // 3 2 5 7 (21, 20, 29)
    /// // 7 5 12 17 (119, 120, 169)
    /// // 17 12 29 41 (697, 696, 985)
    /// // 41 29 70 99 (4059, 4060, 5741)
    /// ```
    ///
    pub fn berggren_b(&mut self) {
        self.f1 = Mpz::from(&self.f2);
        self.dif = Mpz::from(&self.ctr);
        self.ctr = Mpz::from(&self.f1+&self.dif);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [B. Berggren _(1934)_](https://arxiv.org/pdf/0809.4324.pdf) C branch.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::new(1, 1, 2, 3);
    /// let pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_c();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_c();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_c();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_c();
    /// println!("{} {:?}", fb, pt);
    /// // Output: 
    /// // 1 1 2 3 (3, 4, 5)
    /// // 1 2 3 5 (5, 12, 13)
    /// // 1 3 4 7 (7, 24, 25)
    /// // 1 4 5 9 (9, 40, 41)
    /// // 1 5 6 11 (11, 60, 61)
    /// ```
    ///
    pub fn berggren_c(&mut self) {
        self.dif = Mpz::from(&self.ctr);
        self.ctr = Mpz::from(&self.f1+&self.dif);
        self.f2 = Mpz::from(&self.dif+&self.ctr);
    }

    /// Mutates the struct following the [B. Berggren _(1934)_](https://arxiv.org/pdf/0809.4324.pdf) tree,
    /// becoming the parent (tree) value.
    ///
    /// ```rust
    /// use fibbox::FibBox;
    /// let mut fb = FibBox::Root();
    /// let mut pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_c(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_a(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_b(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// fb.berggren_p(); pt = fb.as_pytup();
    /// println!("{} {:?}", fb, pt);
    /// assert!(fb == FibBox::Root());
    /// // Output:
    /// // 1 1 2 3 (3, 4, 5)
    /// // 3 1 4 5 (15, 8, 17)
    /// // 5 4 9 13 (65, 72, 97)
    /// // 5 9 14 23 (115, 252, 277)
    /// // 5 4 9 13 (65, 72, 97)
    /// // 3 1 4 5 (15, 8, 17)
    /// // 1 1 2 3 (3, 4, 5)
    /// ```
    pub fn berggren_p(&mut self) {
        let two = Mpz::from(2);
        if self != &FibBox::Root() {
            if &self.f1 < &self.dif {
                // c branch
                self.ctr = Mpz::from(&self.dif);
                self.dif = Mpz::from(&self.ctr-&self.f1);
                self.f2 = Mpz::from(&self.dif+&self.ctr);
            } else {
                let mut _gcd = (Mpz::from(&self.f1)).gcd(&self.f2);
                let mut _delta = Mpz::from(0);
                if _gcd != 1 {
                    let mut _f1 = (Mpz::from(&self.f1)).div_exact(&_gcd);
                    let mut _f2 = (Mpz::from(&self.f2)).div_exact(&_gcd);
                    let mut _dif = (Mpz::from(&_f2-&_f1)).div_exact(&two);
                    //let mut _ctr = Mpz::from(&_f1+&_dif); // Not needed.
                    _delta = Mpz::from(&_f1-&_dif);
                } else {
                    _delta = Mpz::from(&self.f1-&self.dif);
                }
                match (&_delta, &self.dif) {
                    (_a, _b) if _a > _b => {
                        self.f2 = Mpz::from(&self.f1);
                        self.ctr = Mpz::from(&self.f2-&self.dif);
                        self.f1 = Mpz::from(&self.ctr-&self.dif);
                    },
                    (_a, _b) if _a < _b => {
                        self.ctr = Mpz::from(&self.dif);
                        self.f2 = Mpz::from(&self.f1);
                        self.dif = Mpz::from(&self.f2-&self.ctr);
                        self.f1 = Mpz::from(&self.ctr-&self.dif);
                    },
                    (_a, _b) if _a == _b => panic!("Invalid comparison; equality not allowed."),
                    _ => panic!("Incomplete code!"),
                };
            }
        }
    }
}

