# FibBox

A small library dealing with [Pythagorean triples](https://en.wikipedia.org/wiki/Pythagorean_triple), their factors, and [Difference of two squares](https://en.wikipedia.org/wiki/Difference_of_two_squares).

Allows for iteration of two known (_ternary_) trees representing all primitive Pythagorean triples; see [The Pythagorean Tree: A New Species _(Price, H. Lee; September 2008)_](https://arxiv.org/pdf/0809.4324.pdf).