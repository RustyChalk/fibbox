
use rand::prelude::*;
use rug::Integer;
use fibbox::FibBox;

type Mpz = Integer;

/* Depreciated, see `fibbox_new_tests()`
#[test]
fn new_fibbox() {
    let _fb = FibBox::new(1, 1, 2, 3);
}
*/

#[test]
fn fibbox_new_tests() {
    let v = vec![
        ((1, 1, 2, 3), (3, 4, 5)),
        ((1, 2, 3, 5), (5, 12, 13)),
        ((1, 3, 4, 7), (7, 24, 25)),
        ((1, 4, 5, 9), (40, 9, 41)),
        ((1, 5, 6, 11), (60, 11, 61)),
        ((1, 6, 7, 13), (84, 13, 85)),
        ((1, 7, 8, 15), (112, 15, 113)),
        ((3, 1, 4, 5), (15, 8, 17)),
        ((3, 2, 5, 7), (21, 20, 29)),
        ((3, 4, 7, 11), (33, 56, 65)),
        ((3, 5, 8, 13), (39, 80, 89)),
        ((5, 1, 6, 7), (35, 12, 37)),
        ((5, 2, 7, 9), (45, 28, 53)),
        ((5, 3, 8, 11), (55, 48, 73)),
        ((5, 4, 9, 13), (65, 72, 97)),
        ((7, 1, 8, 9), (16, 63, 65)),
        ((7, 2, 9, 11), (77, 36, 85)),
        ((7, 5, 12, 17), (119, 120, 169)),
    ];
    for i in v {
        let a = FibBox::new((i.0).0, (i.0).1, (i.0).2, (i.0).3);
        let b = FibBox::new_from_tuple(i.0);
        let c = FibBox::new_from_pytrip((i.1).0, (i.1).1, (i.1).2);
        let d = FibBox::new_from_pytrip_tuple(i.1);
        assert_eq!(a, b);
        assert_eq!(b, c);
        assert_eq!(c, d);
        assert!(a.is_primitive());
        assert!(b.is_primitive());
        assert!(c.is_primitive());
        assert!(d.is_primitive());
        if ((&i.1).0)%2 == 1 {
            let e = (Mpz::from((i.1).0), Mpz::from((i.1).1), Mpz::from((i.1).2));
            assert_eq!(a.as_pytup(), e);
            assert_eq!(b.as_pytup(), e);
        } else {
            let e = (Mpz::from((i.1).1), Mpz::from((i.1).0), Mpz::from((i.1).2));
            assert_eq!(a.as_pytup(), e);
            assert_eq!(b.as_pytup(), e);
        }
    }
}

#[test]
fn fibbox_root() {
    let root = FibBox::Root();
    let fb = FibBox::new(1, 1, 2, 3);
    assert_eq!(root, fb);
}

#[test]
fn fibbox_root_limit() {
    let mut root = FibBox::Root();
    let mut fb = FibBox::new(1, 1, 2, 3);
    fb.price_p();
    assert_eq!(root, fb);
    root.berggren_p();
    assert_eq!(root, fb);
}

#[test]
fn fibbox_price_a() {
    let mut fb = FibBox::Root();
    fb.price_a();
    let pa = FibBox::new(1, 2, 3, 5);
    assert_eq!(fb, pa);
    fb.price_a();
    let mut paa = FibBox::new(1, 4, 5, 9);
    assert_eq!(fb, paa);
    paa.price_p();
    assert_eq!(pa, paa);
}

#[test]
fn fibbox_price_b() {
    let mut fb = FibBox::Root();
    fb.price_b();
    let pb = FibBox::new(3, 1, 4, 5);
    assert_eq!(fb, pb);
    fb.price_b();
    let mut pbb = FibBox::new(5, 3, 8, 11);
    assert_eq!(fb, pbb);
    pbb.price_p();
    assert_eq!(pb, pbb);
}

#[test]
fn fibbox_price_c() {
    let mut fb = FibBox::Root();
    fb.price_c();
    let pc = FibBox::new(1, 3, 4, 7);
    assert_eq!(fb, pc);
    fb.price_c();
    let mut pcc = FibBox::new(1, 7, 8, 15);
    assert_eq!(fb, pcc);
    pcc.price_p();
    assert_eq!(pc, pcc);
}

#[test]
fn fibbox_price_raa() {
    let mut fb = FibBox::Root();
    fb.price_a();
    fb.price_a();
    let mut tst = FibBox::new(1, 4, 5, 9);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rab() {
    let mut fb = FibBox::Root();
    fb.price_a();
    fb.price_b();
    let mut tst = FibBox::new(5, 1, 6, 7);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rac() {
    let mut fb = FibBox::Root();
    fb.price_a();
    fb.price_c();
    let mut tst = FibBox::new(1, 5, 6, 11);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rba() {
    let mut fb = FibBox::Root();
    fb.price_b();
    fb.price_a();
    let mut tst = FibBox::new(3, 2, 5, 7);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root(), "{:?} {:?}", tst, FibBox::Root());
}

#[test]
fn fibbox_price_rbb() {
    let mut fb = FibBox::Root();
    fb.price_b();
    fb.price_b();
    let mut tst = FibBox::new(5, 3, 8, 11);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rbc() {
    let mut fb = FibBox::Root();
    fb.price_b();
    fb.price_c();
    let mut tst = FibBox::new(3, 5, 8, 13);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rca() {
    let mut fb = FibBox::Root();
    fb.price_c();
    fb.price_a();
    let mut tst = FibBox::new(1, 6, 7, 13);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rcb() {
    let mut fb = FibBox::Root();
    fb.price_c();
    fb.price_b();
    let mut tst = FibBox::new(7, 1, 8, 9);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_price_rcc() {
    let mut fb = FibBox::Root();
    fb.price_c();
    fb.price_c();
    let mut tst = FibBox::new(1, 7, 8, 15);
    assert_eq!(fb, tst);
    tst.price_p();
    tst.price_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_a() {
    let mut fb = FibBox::Root();
    fb.berggren_a();
    let ba = FibBox::new(3, 1, 4, 5);
    assert_eq!(fb, ba);
    fb.berggren_a();
    let mut baa = FibBox::new(5, 1, 6, 7);
    assert_eq!(fb, baa);
    baa.berggren_p();
    assert_eq!(ba, baa);
}

#[test]
fn fibbox_berggren_b() {
    let mut fb = FibBox::Root();
    fb.berggren_b();
    let bb = FibBox::new(3, 2, 5, 7);
    assert_eq!(fb, bb);
    fb.berggren_b();
    let mut bbb = FibBox::new(7, 5, 12, 17);
    assert_eq!(fb, bbb);
    bbb.berggren_p();
    assert_eq!(bb, bbb);
}

#[test]
fn fibbox_berggren_c() {
    let mut fb = FibBox::Root();
    fb.berggren_c();
    let bc = FibBox::new(1, 2, 3, 5);
    assert_eq!(fb, bc);
    fb.berggren_c();
    let mut bcc = FibBox::new(1, 3, 4, 7);
    assert_eq!(fb, bcc);
    bcc.berggren_p();
    assert_eq!(bc, bcc);
}

#[test]
fn fibbox_berggren_raa() {
    let mut fb = FibBox::Root();
    fb.berggren_a();
    fb.berggren_a();
    let mut tst = FibBox::new(5, 1, 6, 7);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rab() {
    let mut fb = FibBox::Root();
    fb.berggren_a();
    fb.berggren_b();
    let mut tst = FibBox::new(5, 4, 9, 13);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rac() {
    let mut fb = FibBox::Root();
    fb.berggren_a();
    fb.berggren_c();
    let mut tst = FibBox::new(3, 4, 7, 11);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rba() {
    let mut fb = FibBox::Root();
    fb.berggren_b();
    fb.berggren_a();
    let mut tst = FibBox::new(7, 2, 9, 11);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rbb() {
    let mut fb = FibBox::Root();
    fb.berggren_b();
    fb.berggren_b();
    let mut tst = FibBox::new(7, 5, 12, 17);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rbc() {
    let mut fb = FibBox::Root();
    fb.berggren_b();
    fb.berggren_c();
    let mut tst = FibBox::new(3, 5, 8, 13);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rca() {
    let mut fb = FibBox::Root();
    fb.berggren_c();
    fb.berggren_a();
    let mut tst = FibBox::new(5, 2, 7, 9);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rcb() {
    let mut fb = FibBox::Root();
    fb.berggren_c();
    fb.berggren_b();
    let mut tst = FibBox::new(5, 3, 8, 11);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn fibbox_berggren_rcc() {
    let mut fb = FibBox::Root();
    fb.berggren_c();
    fb.berggren_c();
    let mut tst = FibBox::new(1, 3, 4, 7);
    assert_eq!(fb, tst);
    tst.berggren_p();
    tst.berggren_p();
    assert_eq!(tst, FibBox::Root());
}

#[test]
fn price_random_children_to_root() {
    // Random number generation through `rug` doesn't play nice (not random).
    // See [dev-dependencies] in 'Cargo.toml' -> `rand`
    let mut rng = rand::thread_rng();
    let mut fb = FibBox::Root();
    let mut path = String::from("");
    let mut ctr = 0;
    for _i in 0..1000 {
        let r: u64 = rng.gen();
        match r%3 {
            0 => {
                path.push('a');
                fb.price_a();
            },
            1 => {
                path.push('b');
                fb.price_b();
            },
            2 => {
                path.push('c');
                fb.price_c();
            },
            _ => panic!("Illogicial."), 
        };
        ctr += 1;
        if ctr%100 == 0 {
           path.push('\n'); 
        }
    }
    println!("\nPrice Random Path: \n{}", path);
    // see it, believe it, check it
    // bit-length, base-2 (radix)
    println!("f1: {} ({} bits)", fb.f1(), fb.f1().to_string_radix(2).len());
    println!("dif: {} ({} bits)", fb.dif(), fb.dif().to_string_radix(2).len());
    println!("ctr: {} ({} bits)", fb.ctr(), fb.ctr().to_string_radix(2).len());
    println!("f2: {} ({} bits)", fb.f2(), fb.f2().to_string_radix(2).len());
    println!("A: {} ({} bits)", fb.as_pytup().0, (&fb.as_pytup().0).to_string_radix(2).len());
    println!("B: {} ({} bits)", fb.as_pytup().1, (&fb.as_pytup().1).to_string_radix(2).len());
    println!("C: {} ({} bits)", fb.as_pytup().2, (&fb.as_pytup().2).to_string_radix(2).len());
    for _i in 0..1000 {
        fb.price_p();
    }
    assert_eq!(fb, FibBox::Root());
}

#[test]
fn berggren_random_children_to_root() {
    // Random number generation through `rug` doesn't play nice (not random).
    // See [dev-dependencies] in 'Cargo.toml' -> `rand`
    let mut rng = rand::thread_rng();
    let mut fb = FibBox::Root();
    let mut path = String::from("");
    let mut ctr = 0;
    for _i in 0..1000 {
        let r: u64 = rng.gen();
        match r%3 {
            0 => {
                path.push('a');
                fb.berggren_a();
            },
            1 => {
                path.push('b');
                fb.berggren_b();
            },
            2 => {
                path.push('c');
                fb.berggren_c();
            },
            _ => panic!("Illogicial."), 
        };
        ctr += 1;
        if ctr%100 == 0 {
           path.push('\n'); 
        }
    }
    println!("\nBerggren Random Path: \n{}\n", path);
    // see it, believe it, check it
    // bit-length, base-2 (radix)
    println!("f1: {} ({} bits)", fb.f1(), fb.f1().to_string_radix(2).len());
    println!("dif: {} ({} bits)", fb.dif(), fb.dif().to_string_radix(2).len());
    println!("ctr: {} ({} bits)", fb.ctr(), fb.ctr().to_string_radix(2).len());
    println!("f2: {} ({} bits)", fb.f2(), fb.f2().to_string_radix(2).len());
    println!("A: {} ({} bits)", fb.as_pytup().0, (&fb.as_pytup().0).to_string_radix(2).len());
    println!("B: {} ({} bits)", fb.as_pytup().1, (&fb.as_pytup().1).to_string_radix(2).len());
    println!("C: {} ({} bits)", fb.as_pytup().2, (&fb.as_pytup().2).to_string_radix(2).len());
    for _i in 0..1000 {
        fb.berggren_p();
    }
    assert_eq!(fb, FibBox::Root());
}

#[test]
#[should_panic(expected = "not primitive!")]
fn fibbox_not_primitive() {
    let np = FibBox::new(9, 3, 12, 15); // (135, 72, 153)
    assert!(np.is_primitive(), "not primitive!");
}

#[test]
fn fibbox_is_primitive() {
    let fb = FibBox::new(1, 2, 3, 5); // (5, 12, 13)
    assert!(fb.is_primitive(), "not primitive!");
}

#[test]
fn fibbox_as_tuple() {
    let fb = FibBox::new(1, 2, 3, 5); // (5, 12, 13)
    assert_eq!(fb.as_tuple(), (Mpz::from(1), Mpz::from(2), Mpz::from(3), Mpz::from(5)));
}

#[test]
fn fibbox_as_pytrip() {
    let fb = FibBox::new(1, 2, 3, 5); // (5, 12, 13)
    assert_eq!(fb.as_pytup(), (Mpz::from(5), Mpz::from(12), Mpz::from(13)));
}

#[test]
fn fibbox_gcd() {
    let np = FibBox::new(9, 3, 12, 15); // (135, 72, 153)
    assert_eq!(np.gcd(), 3);
}

#[test]
fn fibbox_f1_dif_ctr_f2() {
    let fb = FibBox::new(1, 2, 3, 5); // (5, 12, 13)
    assert_eq!(fb.f1(), &Mpz::from(1));
    assert_eq!(fb.dif(), &Mpz::from(2));
    assert_eq!(fb.ctr(), &Mpz::from(3));
    assert_eq!(fb.f2(), &Mpz::from(5));
}

